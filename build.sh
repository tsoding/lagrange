#!/bin/sh

set -xe

CC=cc
CFLAGS="-Wall -Wextra -Wshadow -std=c11 -pedantic -ggdb `pkg-config --cflags sdl2`"
LIBS="`pkg-config --libs sdl2` -lm"

$CC $CFLAGS -o main main.c $LIBS
