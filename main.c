#include <assert.h>
#include <stdio.h>
#include <stdbool.h>

#include <SDL.h>

#define HEXCOLOR(code) \
  ((code) >> (3 * 8)) & 0xFF, \
  ((code) >> (2 * 8)) & 0xFF, \
  ((code) >> (1 * 8)) & 0xFF, \
  ((code) >> (0 * 8)) & 0xFF

#define POINT_SIZE 30
#define POINT_COLOR 0xFF1818FF

SDL_Rect rect_of_point(SDL_Point point)
{
    SDL_Rect rect = {
        .x = point.x - POINT_SIZE/2,
        .y = point.y - POINT_SIZE/2,
        .w = POINT_SIZE,
        .h = POINT_SIZE,
    };
    return rect;
}

void render_point(SDL_Renderer *renderer, SDL_Point point, Uint32 color)
{
    SDL_Rect rect = rect_of_point(point);
    SDL_SetRenderDrawColor(renderer, HEXCOLOR(color));
    SDL_RenderFillRect(renderer, &rect);
}

#define POINTS_CAP 1024
SDL_Point points[POINTS_CAP];
size_t points_sz = 0;

void add_point(int x, int y)
{
    assert(points_sz < POINTS_CAP);
    points[points_sz].x = x;
    points[points_sz].y = y;
    points_sz += 1;
}

void swap_points(size_t index0, size_t index1)
{
    SDL_Point t = points[index0];
    points[index0] = points[index1];
    points[index1] = t;
}

void delete_point(size_t index)
{
    assert(index < points_sz);
    swap_points(index, points_sz-1);
    points_sz--;
}

int point_at(int at_x, int at_y)
{
    SDL_Point at_point = {
        .x = at_x,
        .y = at_y,
    };
    for (size_t i = 0; i < points_sz; ++i) {
        SDL_Rect rect = rect_of_point(points[i]);
        if (SDL_PointInRect(&at_point, &rect)) {
            return (int) i;
        }
    }
    return -1;
}

float lagrange_basis(float x, size_t j, SDL_Point *ps, size_t ps_sz)
{
    float y = 1.0f;
    for (size_t m = 0; m < ps_sz; ++m) {
        if (m != j) {
            y *= (x - (float) ps[m].x) / ((float) ps[j].x - (float) ps[m].x);
        }
    }
    return y;
}

float lagrange_poly(float x, SDL_Point *ps, size_t ps_sz)
{
    float y = 0.0f;
    for (size_t j = 0; j < ps_sz; ++j) {
        y += ps[j].y * lagrange_basis(x, j, ps, ps_sz);
    }
    return y;
}

#define INTERPOLATION_STEP 10
#define INTERPOLATION_COLOR 0x18FF18FF

int main(void)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "ERROR: could not initialize SDL: %s\n",
                SDL_GetError());
        exit(1);
    }

    SDL_Window * window = SDL_CreateWindow(
                              "Lagrange Interpolation",
                              0, 0,
                              800, 600,
                              SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        fprintf(stderr, "ERROR: could not create a window: %s\n",
                SDL_GetError());
        exit(1);
    }

    SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == NULL) {
        fprintf(stderr, "ERROR: could not create a renderer: %s\n",
                SDL_GetError());
        exit(1);
    }

    bool quit = false;
    int drag_index = -1;
    while (!quit) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                quit = true;
                break;

            case SDL_MOUSEBUTTONDOWN: {
                switch (event.button.button) {
                case SDL_BUTTON_LEFT: {
                    int index = point_at(event.button.x, event.button.y);
                    if (index < 0) {
                        add_point(event.button.x, event.button.y);
                    } else {
                        drag_index = index;
                    }
                }
                break;

                case SDL_BUTTON_RIGHT: {
                    int index = point_at(event.button.x, event.button.y);
                    if (index >= 0) {
                        delete_point((size_t) index);
                    }
                } break;
                }
            }
            break;

            case SDL_MOUSEMOTION: {
                if (drag_index >= 0) {
                    points[drag_index].x = event.button.x;
                    points[drag_index].y = event.button.y;
                }
            }
            break;

            case SDL_MOUSEBUTTONUP: {
                drag_index = -1;
            }
            break;
            }
        }

        SDL_SetRenderDrawColor(renderer, HEXCOLOR(0x181818FF));
        SDL_RenderClear(renderer);

        if (points_sz >= 2) {
            for (float x = points[0].x; x <= points[points_sz-1].x; x += INTERPOLATION_STEP) {
                float y = lagrange_poly(x, points, points_sz);
                SDL_Point point = {
                    .x = (int) floorf(x),
                    .y = (int) floorf(y),
                };
                render_point(renderer, point, INTERPOLATION_COLOR);
            }
        }

        for (size_t i = 0; i < points_sz; ++i) {
            render_point(renderer, points[i], POINT_COLOR);
        }

        SDL_RenderPresent(renderer);
    }

    SDL_Quit();
    return 0;
}
